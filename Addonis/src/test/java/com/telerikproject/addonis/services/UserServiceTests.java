package com.telerikproject.addonis.services;

import com.telerikproject.addonis.Helpers;
import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    com.telerikproject.addonis.repositories.interfaces.UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }




    @Test
    public void getById_should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser =Helpers.createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result=service.getById(mockUser.getId());


        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(),result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(),result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(),result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(),result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(),result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getType().getId(),result.getType().getId())
        );

    }


    @Test
    public void getByUsername_should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser =Helpers.createMockUser();
        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        User result=service.getByUsername(mockUser.getUsername());


        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(),result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(),result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(),result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(),result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(),result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getType().getId(),result.getType().getId())
        );

    }


    @Test
    public void getByEmail_should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser =Helpers.createMockUser();
        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        User result=service.getByEmail(mockUser.getEmail());


        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(),result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(),result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(),result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(),result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(),result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getType().getId(),result.getType().getId())
        );

    }


    @Test
    public void getByPhoneNumber_should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser =Helpers.createMockUser();
        Mockito.when(mockRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenReturn(mockUser);

        User result=service.getByPhoneNumber(mockUser.getPhoneNumber());


        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(),result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(),result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(),result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(),result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(),result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getType().getId(),result.getType().getId())
        );

    }

    @Test
    public void create_Should_Throw_When_UserWithSameEmailExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockUser));

    }

    @Test
    public void create_Should_Throw_When_UserWithSameUsernameExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockUser));
    }

    @Test
    public void create_Should_Throw_When_UserWithSamePhoneNumberExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockUser));
    }


    @Test
    public void create_should_callRepository_When_User_WithSame_Username_OrSameEmail_orSamePhoneNumber_NotExist() {
        // Arrange
        var mockUser = Helpers.createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));


        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));

        Mockito.when(mockRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));



        // Act
        service.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }





    @Test
    public void update_Should_Throw_When_UserWithSameEmailExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockUser));

    }

    @Test
    public void update_Should_Throw_When_UserWithSameUsernameExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockUser));
    }

    @Test
    public void update_Should_Throw_When_UserWithSamePhoneNumberExists(){
        //Arrange
        var mockUser= Helpers.createMockUser();

        Mockito.when(mockRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenReturn(mockUser);


        //act,Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockUser));
    }


    @Test
    public void update_Should_CallRepository_When_User_WithSame_Username_OrSameEmail_orSamePhoneNumber_NotExist(){
        //!!
        //Arrange
        var mockUser=Helpers.createMockUser();


        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));


        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));

        Mockito.when(mockRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenThrow(new EntityNotFoundException("User", "Email/Username/phoneNumber", mockUser.getPhoneNumber()));


        //Act
        service.update(mockUser);

        //Assert
        Mockito.verify(mockRepository,Mockito.times(1))
                .update(Mockito.any(User.class));
    }






}
