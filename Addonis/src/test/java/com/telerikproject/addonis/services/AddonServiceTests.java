package com.telerikproject.addonis.services;

import com.telerikproject.addonis.Helpers;
import com.telerikproject.addonis.models.Addon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class AddonServiceTests {

    @Mock
    com.telerikproject.addonis.repositories.interfaces.AddonRepository mockRepository;

    @InjectMocks
    AddonServiceImpl service;


    @Test
    public void getById_should_ReturnAddon_When_MatchExists(){
        //Arrange
        Addon mockAddon = Helpers.createMockAddon();
        Mockito.when(mockRepository.getById(mockAddon.getId()))
                .thenReturn(mockAddon);

        Addon result=service.getById(mockAddon.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockAddon.getId(),result.getId()),
                () -> Assertions.assertEquals(mockAddon.getName(),result.getName()),
                () -> Assertions.assertEquals(mockAddon.getIde(),result.getIde()),
                () -> Assertions.assertEquals(mockAddon.getDescription(),result.getDescription()),
                () -> Assertions.assertEquals(mockAddon.getOrigin(),result.getOrigin()),
                () -> Assertions.assertEquals(mockAddon.getNumberOfDownloads(),result.getNumberOfDownloads()),
                () -> Assertions.assertEquals(mockAddon.getRating(),result.getRating()),
                () -> Assertions.assertEquals(mockAddon.getUploadDate(),result.getUploadDate()),
                () -> Assertions.assertEquals(mockAddon.getCreatedBy(),result.getCreatedBy())
        );

    }
    @Test
    public void getAll_Should_CallRepository(){
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        service.getAll();

        Mockito.verify(mockRepository,Mockito.times(1))
                .getAll();

    }

    @Test
    public void create_Should_CallRepository(){
        //Arrange
        Addon mockAddon= Helpers.createMockAddon();

        //Act
        service.create(mockAddon);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockAddon);
    }

    @Test
    public void update_Should_CallRepository(){
        //Arrange
        Addon mockAddon = Helpers.createMockAddon();

        //Act
        service.update(mockAddon);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockAddon);
    }
}
