package com.telerikproject.addonis;

import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.UserType;

import java.time.LocalDateTime;
import java.util.Set;

public class Helpers {


    public static User createMockAdmin() {
        return createMockUser(1);
    }

    public static User createMockUser() {
        return createMockUser(2);
    }

    private static User createMockUser(int typeId) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("mockUsername");
        mockUser.setPassword("mockPassword");
        mockUser.setEmail("mock@user.com");
        mockUser.setPhoneNumber("mockPhone");
        mockUser.setType(createMockType(typeId));
        mockUser.setBlocked(false);
        return mockUser;
    }

    public static Addon createMockAddon(){
        var mockAddon = new Addon();
        mockAddon.setId(1);
        mockAddon.setSelectedByAdmin(false);
        mockAddon.setType("mockType");
        mockAddon.setName("mockName");
        mockAddon.setIde(createMockIde());
        mockAddon.setDescription("mockDescription");
        mockAddon.setOrigin("mockOrigin");
        mockAddon.setNumberOfDownloads(0);
        mockAddon.setRating(1);
        mockAddon.setUploadDate(LocalDateTime.now());
        mockAddon.setCreatedBy(createMockUser());
        return mockAddon;
    }


    private static UserType createMockType(int id) {
        var mockUserType = new UserType();
        mockUserType.setId(id);
        mockUserType.setName("mockType");
        return mockUserType;
    }

    private static IDE createMockIde(){
        var mockIde = new IDE();
        mockIde.setId(1);
        mockIde.setName("mockIde");
        return mockIde;
    }


}
