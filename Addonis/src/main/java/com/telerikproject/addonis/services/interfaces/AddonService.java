package com.telerikproject.addonis.services.interfaces;

import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.User;

import java.util.List;
import java.util.Optional;

public interface AddonService {

    List<Addon> getAll();

    Addon getById(int id);

    Addon getByName(String name);

    List<Addon> getForUser(String username);

    List<Addon> getPendingAddongsForUser(String username);

    void create(Addon addon);

    void update(Addon addon);

    void delete(String name, User user);

    List<Addon> filter(Optional<String> addonName,
                       Optional<Integer> ideId,
                       Optional<String> sortParams);

    public List<Addon> showMostPopular();

    public List<Addon> showFeatured();

    public List<Addon> showNewest();

    void changeApprovalStatus(Addon addonToUpdate);

    void changeFeaturedStatus(Addon addonToUpdate);

    public byte[] downloadFile(Addon addon);
}
