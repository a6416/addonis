package com.telerikproject.addonis.services.interfaces;

import com.telerikproject.addonis.models.Tag;

import java.util.List;

public interface TagService {

    void create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    void update(Tag tag);


}
