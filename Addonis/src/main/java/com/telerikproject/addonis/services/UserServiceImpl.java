package com.telerikproject.addonis.services;

import com.telerikproject.addonis.exceptions.AuthenticationFailureException;
import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.UserType;
import com.telerikproject.addonis.repositories.interfaces.AddonRepository;
import com.telerikproject.addonis.repositories.interfaces.UserRepository;
import com.telerikproject.addonis.repositories.interfaces.UserTypeRepository;
import com.telerikproject.addonis.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {


    public static final String PASSWORD_ERROR = "Password must contain at least one number, one lowercase, one uppercase and one special characters and have a minimum length of 3 and maximum length of 20.";
    private final UserRepository repository;
    private final UserTypeRepository userTypeRepository;
    private final AddonRepository addonRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, UserTypeRepository userTypeRepository, AddonRepository addonRepository) {
        this.repository = repository;
        this.userTypeRepository = userTypeRepository;
        this.addonRepository = addonRepository;
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        return repository.getByPhoneNumber(phoneNumber);
    }

    @Override
    public void checkPassword(String password) {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[!\"?£()@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        if (!p.matcher(password).matches()) {
            throw new AuthenticationFailureException(PASSWORD_ERROR);
        }
    }

    @Override
    public void create(User user) {
        checkForDuplicatesUsername(user.getUsername());
        checkForDuplicatesEmail(user.getEmail());
        checkForDuplicatesPhoneNumber(user.getPhoneNumber());

        repository.create(user);
    }

    @Override
    public void update(User user) {
        boolean duplicateEmail = true;
        boolean duplicatePhoneNumber = true;

        try {
            User existingUser = repository.getByPhoneNumber(user.getPhoneNumber());
            if (user.getId() == existingUser.getId()) {
                duplicatePhoneNumber = false;
            }
        } catch (EntityNotFoundException e) {
            duplicatePhoneNumber = false;
        }

        try {
            User existingUser = repository.getByEmail(user.getEmail());
            if (user.getId() == existingUser.getId()) {
                duplicateEmail = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmail = false;
        }


        if (!duplicateEmail && !duplicatePhoneNumber) {
            repository.update(user);
        }


    }


    @Override
    public void checkForDuplicatesEmail(String email) {
        try {
            repository.getByEmail(email);
            throw new DuplicateEntityException("User", "email", email);
        } catch (EntityNotFoundException ignored) {
        }

    }

    @Override
    public void checkForDuplicatesUsername(String username) {
        try {
            repository.getByUsername(username);
            throw new DuplicateEntityException("User", "username", username);
        } catch (EntityNotFoundException ignored) {
        }
    }

    @Override
    public void checkForDuplicatesPhoneNumber(String phoneNumber) {

        try {
            repository.getByPhoneNumber(phoneNumber);
            throw new DuplicateEntityException("User", "phone number", phoneNumber);
        } catch (EntityNotFoundException ignored) {
        }
    }

    @Override
    public void delete(User user) {
        if (!addonRepository.getForUser(user.getUsername()).isEmpty()) {
            addonRepository.getForUser(user.getUsername()).forEach(addon -> addonRepository.delete(addon.getId()));
        }
        repository.delete(user.getId());

    }

    @Override
    public void changeAdminStatus(User user) {
        UserType adminType = userTypeRepository.getById(1);
        UserType userType = userTypeRepository.getById(2);

        if (user.getType().getId() == 1) {
            user.setType(userType);
        } else user.setType(adminType);

        update(user);
    }


}
