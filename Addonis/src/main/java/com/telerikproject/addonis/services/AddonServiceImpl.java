package com.telerikproject.addonis.services;

import com.telerikproject.addonis.domain.AddonCommits;
import com.telerikproject.addonis.domain.AddonOpenIssues;
import com.telerikproject.addonis.domain.AddonPullRequests;
import com.telerikproject.addonis.exceptions.*;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.Rating;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.repositories.interfaces.AddonDownloadRepository;
import com.telerikproject.addonis.repositories.interfaces.AddonRepository;
import com.telerikproject.addonis.repositories.interfaces.IDERepository;
import com.telerikproject.addonis.repositories.interfaces.RatingRepository;
import com.telerikproject.addonis.services.interfaces.AddonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class AddonServiceImpl implements AddonService {

    public static final String DUPLICATE_ADDON = "Addon with this name or origin link already exists!";
    public static final String NEEDED_FILE_TYPE = "application/x-zip-compressed";

    private final AddonRepository addonRepository;
    private final RatingRepository ratingRepository;
    private final IDERepository ideRepository;
    private final AddonDownloadRepository addonDownloadRepository;

    @Autowired
    public AddonServiceImpl(AddonRepository repository, RatingRepository ratingRepository, IDERepository ideRepository, AddonDownloadRepository addonDownloadRepository) {
        this.addonRepository = repository;
        this.ratingRepository = ratingRepository;
        this.ideRepository = ideRepository;
        this.addonDownloadRepository = addonDownloadRepository;
    }


    @Override
    public List<Addon> getAll() {
        return addonRepository.getAll();
    }

    @Override
    public Addon getById(int id) {
        return addonRepository.getById(id);
    }

    @Override
    public Addon getByName(String name) {
        return addonRepository.getByName(name);
    }

    @Override
    public List<Addon> getForUser(String username) {
        return addonRepository.getForUser(username);
    }

    @Override
    public List<Addon> getPendingAddongsForUser(String username) {
        return addonRepository.getForUser(username).stream().filter(addon -> !addon.isApproved()).collect(Collectors.toList());
    }

    @Override
    public void create(Addon addon) {
        boolean duplicateNameExists = true;
        boolean duplicateOriginExists = true;

        try {
            addonRepository.getByName(addon.getName());
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }

        try {
            addonRepository.getByOrigin(addon.getOrigin());
        } catch (EntityNotFoundException e) {
            duplicateOriginExists = false;
        }

        if (duplicateNameExists || duplicateOriginExists) {
            throw new DuplicateEntityException(DUPLICATE_ADDON);
        }

        if (!addon.getType().equals(NEEDED_FILE_TYPE)) {
            throw new NotExpectedFileTypeException("zip");
        }

        consumeAPI(addon);
        addonRepository.create(addon);

    }

    @Override
    public void update(Addon addon) {
        try {
            addonRepository.getById(addon.getId());
        } catch (EntityNotFoundException e) {
            return;
        }

        addonRepository.update(addon);
    }

    @Override
    public void delete(String name, User user) {
        Addon addon = addonRepository.getByName(name);
        if (!addon.getCreatedBy().equals(user) && user.getType().getId() != 1) {
            throw new UnauthorizedOperationException();
        }
        addonRepository.delete(addon.getId());
    }

    @Scheduled(fixedRate = 3600000)
    private void consumeAPI() {
        HttpEntity<String> entity = generateHttpEntity();

        List<Addon> addons = getAll();

        for (Addon addon :
                addons) {
            String url = generateURL(addon.getOrigin());
            consumeOpenIssuesCount(addon, url, entity);
            consumePullRequestsCount(addon, url, entity);
            consumeLastCommit(addon, url, entity);
        }
    }

    private void consumeAPI(Addon addon) {
        HttpEntity<String> entity = generateHttpEntity();

        String url = generateURL(addon.getOrigin());
        consumeOpenIssuesCount(addon, url, entity);
        consumePullRequestsCount(addon, url, entity);
        consumeLastCommit(addon, url, entity);

    }

    private HttpEntity<String> generateHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "token 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");
        return new HttpEntity<>(null, headers);
    }

    private void consumeOpenIssuesCount(Addon addon, String url, HttpEntity<String> entity) {
        RestTemplate restTemplate = new RestTemplate();
        AddonOpenIssues addonGit = restTemplate.getForObject(url, AddonOpenIssues.class, entity);

        if (addonGit == null) {
            addon.setOpenIssuesCount(0);
        } else addon.setOpenIssuesCount(addonGit.getOpen_issues_count());

    }

    private void consumePullRequestsCount(Addon addon, String url, HttpEntity<String> entity) {
        RestTemplate restTemplate = new RestTemplate();
        AddonPullRequests[] addonPullRequests = restTemplate.getForObject(url + "/pulls", AddonPullRequests[].class, entity);

        if (addonPullRequests == null) {
            addon.setPullRequestsCount(0);
        } else addon.setPullRequestsCount(addonPullRequests.length);
    }

    private void consumeLastCommit(Addon addon, String url, HttpEntity<String> entity) {
        RestTemplate restTemplate = new RestTemplate();
        AddonCommits[] addonCommits = restTemplate.getForObject(url + "/commits", AddonCommits[].class, entity);

        assert addonCommits != null;
        String dateStr = addonCommits[0].getCommit().getCommitter().getDate().substring(0, 10);

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(dateStr);
            addon.setLastCommitDate(date);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }

        addon.setLastCommitMessage(addonCommits[0].getCommit().getMessage());
    }

    private String generateURL(String origin) {
        return ("https://api.github.com/repos" + origin.substring(18));
    }

    @Override
    public void changeApprovalStatus(Addon addonToUpdate) {
        addonToUpdate.setApproved(!addonToUpdate.isApproved());
        update(addonToUpdate);
    }

    @Override
    public void changeFeaturedStatus(Addon addonToUpdate) {
        addonToUpdate.setSelectedByAdmin(!addonToUpdate.isSelectedByAdmin());
        update(addonToUpdate);
    }

    @Override
    public List<Addon> filter(Optional<String> addonName,
                              Optional<Integer> ideId,
                              Optional<String> sortParams) {
        return addonRepository.filter(addonName, ideId, sortParams);
    }

    public List<Addon> showMostPopular() {
        return addonRepository.getAll()
                .stream()
                .sorted(Comparator.comparing(Addon::getNumberOfDownloads))
                .limit(6)
                .collect(Collectors.toList());
    }

    public List<Addon> showFeatured() {
        return addonRepository.getAll()
                .stream()
                .filter(Addon::isSelectedByAdmin)
                .collect(Collectors.toList());
    }

    public List<Addon> showNewest() {
        return addonRepository.getAll()
                .stream()
                .sorted(Comparator.comparing(Addon::getUploadDate))
                .limit(6)
                .collect(Collectors.toList());
    }

    @Override
    public byte[] downloadFile(Addon addon) {
        try {
            addon.setNumberOfDownloads(addon.getNumberOfDownloads() + 1);
            return zipBytes(addon.getFileName(), addon.getData());
        } catch (IOException e) {
            throw new UnexpectedErrorException();
        }
    }

    public static byte[] zipBytes(String filename, byte[] input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        ZipEntry entry = new ZipEntry(filename);
        entry.setSize(input.length);
        zos.putNextEntry(entry);
        zos.write(input);
        zos.closeEntry();
        zos.close();
        return baos.toByteArray();
    }

}
