package com.telerikproject.addonis.services.interfaces;

import com.telerikproject.addonis.models.Rating;

import java.util.List;

public interface RatingService {

    void create(Rating rating);

    List<Rating> getAll();

    List<Rating> getRatingsForAddon(int id);

    void update(Rating rating);

    void delete(int addonId, int userId);
}
