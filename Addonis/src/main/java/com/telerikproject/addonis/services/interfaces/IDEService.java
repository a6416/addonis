package com.telerikproject.addonis.services.interfaces;

import com.telerikproject.addonis.models.IDE;

import java.util.List;

public interface IDEService {

    void create(IDE ide);

    List<IDE> getAll();

    IDE getByName(String name);

    void delete(String name);
}
