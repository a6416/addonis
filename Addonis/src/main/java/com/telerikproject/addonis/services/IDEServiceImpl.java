package com.telerikproject.addonis.services;

import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.repositories.interfaces.IDERepository;
import com.telerikproject.addonis.services.interfaces.IDEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IDEServiceImpl implements IDEService {

    private final IDERepository ideRepository;

    @Autowired
    public IDEServiceImpl(IDERepository ideRepository) {
        this.ideRepository = ideRepository;
    }


    @Override
    public void create(IDE ide) {
        boolean duplicateExists = true;
        try {
            ideRepository.getByName(ide.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (!duplicateExists) {
            ideRepository.create(ide);
        }
    }

    @Override
    public List<IDE> getAll() {
        return ideRepository.getAll();
    }

    @Override
    public IDE getByName(String name) {
        return ideRepository.getByName(name);
    }

    @Override
    public void delete(String name) {
        ideRepository.delete(name);
    }
}
