package com.telerikproject.addonis.services;

import com.telerikproject.addonis.models.Rating;
import com.telerikproject.addonis.repositories.interfaces.RatingRepository;
import com.telerikproject.addonis.services.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void create(Rating rating) {
        ratingRepository.create(rating);
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getAll();
    }

    @Override
    public List<Rating> getRatingsForAddon(int id) {
        return ratingRepository.getRatingsForAddon(id);
    }

    @Override
    public void update(Rating rating) {
        ratingRepository.update(rating);
    }

    @Override
    public void delete(int addonId, int userId) {
        ratingRepository.delete(addonId, userId);
    }


}
