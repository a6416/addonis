package com.telerikproject.addonis.services;

import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.Tag;
import com.telerikproject.addonis.repositories.interfaces.TagRepository;
import com.telerikproject.addonis.services.interfaces.TagService;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagServiceImpl implements TagService {

    public final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }


    @Override
    public void create(Tag tag) {
        boolean duplicateTag = true;

        try {
            tagRepository.getByName(tag.getTagName());
        } catch (EntityNotFoundException e) {
            duplicateTag = false;
        }
        if (duplicateTag) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }


        tagRepository.create(tag);
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.getById(id);
    }

    @Override
    public void update(Tag tag) {
        tagRepository.update(tag);
    }
}
