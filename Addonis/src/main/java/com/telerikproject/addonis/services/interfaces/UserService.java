package com.telerikproject.addonis.services.interfaces;

import com.telerikproject.addonis.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhoneNumber(String phoneNumber);

    void create(User user);

    void update(User user);

    void delete(User user);

    void checkPassword(String password);

    void checkForDuplicatesEmail(String email);

    void checkForDuplicatesUsername(String username);

    void checkForDuplicatesPhoneNumber(String phoneNumber);

    void changeAdminStatus(User user);
}
