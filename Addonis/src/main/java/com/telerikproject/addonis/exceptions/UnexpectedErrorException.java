package com.telerikproject.addonis.exceptions;

public class UnexpectedErrorException extends RuntimeException {
    public UnexpectedErrorException() {
        super("An unexpected error occurred. Please try again.");
    }
}
