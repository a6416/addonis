package com.telerikproject.addonis.exceptions;

public class NotMatchingEmailException extends RuntimeException {
    public NotMatchingEmailException() {
        super("The email you've entered does not mach the current email");
    }
}
