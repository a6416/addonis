package com.telerikproject.addonis.exceptions;

public class DuplicateEntityException extends RuntimeException {
    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String type, String attribute) {
        super(String.format("%s with this %s already exists.", type, attribute));
    }

    public DuplicateEntityException(String text) {
        super(text);
    }

}
