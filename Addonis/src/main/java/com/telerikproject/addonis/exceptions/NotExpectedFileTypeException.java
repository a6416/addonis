package com.telerikproject.addonis.exceptions;

public class NotExpectedFileTypeException extends RuntimeException {
    public NotExpectedFileTypeException(String expectedType) {
        super(String.format("Your file must be %s", expectedType));
    }
}
