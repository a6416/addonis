package com.telerikproject.addonis.models.mappers;

import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.models.Tag;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.dtos.AddonDto;
import com.telerikproject.addonis.repositories.interfaces.AddonRepository;
import com.telerikproject.addonis.repositories.interfaces.IDERepository;
import com.telerikproject.addonis.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;

@Component
public class AddonMapper {

    private final AddonRepository addonRepository;
    private final IDERepository ideRepository;
    private final TagService tagService;


    @Autowired
    public AddonMapper(AddonRepository addonRepository, IDERepository ideRepository, TagService tagService) {
        this.addonRepository = addonRepository;
        this.ideRepository = ideRepository;
        this.tagService = tagService;
    }

    public Addon fromDto(MultipartFile file, AddonDto addonDto, User user) {
        Addon addon = new Addon();
        dtoToObject(file, addonDto, addon, user);
        return addon;
    }

    public Addon fromDto(MultipartFile file, AddonDto addonDto, int id) {
        Addon addon = addonRepository.getById(id);
        dtoToObject(file, addonDto, addon, addon.getCreatedBy());
        return addon;
    }

    private void dtoToObject(MultipartFile file, AddonDto addonDto, Addon addon, User user) {
        IDE ide = ideRepository.getById(addonDto.getIdeId());
        try {
            addon.setName(addonDto.getName());
            System.out.println();
            addon.setIde(ide);
            addon.setDescription(addonDto.getDescription());
            addon.setOrigin(addonDto.getOrigin());
            addon.setNumberOfDownloads(0);
            addon.setUploadDate(LocalDate.now());
            addon.setCreatedBy(user);
            String arr[] = addonDto.getTags().split(" ");

            for (int i = 0; i < arr.length; i++) {
                Tag tag = new Tag();
                tag.setTagName(arr[i]);
                if (!addon.getTags().contains(tag)) {
                    try {
                        tagService.create(tag);
                    } catch (DuplicateEntityException e) {
                    }
                    ;
                    addon.getTags().add(tag);
                }
            }


            if (user.getType().getId() == 1) {
                addon.setApproved(true);
            } else addon.setApproved(false);

            addon.setData(file.getBytes());
            addon.setFileName(file.getOriginalFilename());
            addon.setType(file.getContentType());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
