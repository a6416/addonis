package com.telerikproject.addonis.models.dtos;

import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

public class RegistrationDto {

    @NotNull
    @Size(min = 2, max = 20, message = "Username must be between 2 and 20 characters.")
    private String username;

    @NotNull
    private String email;

    @NotNull
    @Size(min = 10, max = 10, message = "Phone number must be 10 digits.")
    private String phoneNumber;

    @NotNull
    @Size(min = 8, max = 20)
    private String password;

    @NotNull
    private String passwordConfirm;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
