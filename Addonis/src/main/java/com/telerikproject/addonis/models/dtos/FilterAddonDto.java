package com.telerikproject.addonis.models.dtos;

public class FilterAddonDto {

    private String addonName;

    private int ideId;

    private String sortParams;

    public FilterAddonDto() {
    }

    public FilterAddonDto(String addonName, int ideId, String sortParams) {
        this.addonName = addonName;
        this.ideId = ideId;
        this.sortParams = sortParams;
    }

    public String getAddonName() {
        return addonName;
    }

    public void setAddonName(String addonName) {
        this.addonName = addonName;
    }

    public int getIdeId() {
        return ideId;
    }

    public void setIdeId(int ideId) {
        this.ideId = ideId;
    }

    public String getSortParams() {
        return sortParams;
    }

    public void setSortParams(String sortParams) {
        this.sortParams = sortParams;
    }
}
