package com.telerikproject.addonis.models.mappers;

import com.telerikproject.addonis.controllers.AuthenticationHelper;
import com.telerikproject.addonis.exceptions.NotMatchingEmailException;
import com.telerikproject.addonis.models.dtos.RegistrationDto;
import com.telerikproject.addonis.models.dtos.UpdateUserDto;
import com.telerikproject.addonis.models.dtos.UserDto;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.repositories.interfaces.UserRepository;
import com.telerikproject.addonis.repositories.interfaces.UserTypeRepository;
import com.telerikproject.addonis.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Component
public class UserMapper {

    private final UserTypeRepository userTypeRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMapper(UserTypeRepository userTypeRepository, UserRepository userRepository, UserService userService, AuthenticationHelper authenticationHelper) {
        this.userTypeRepository = userTypeRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }


    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setType(userTypeRepository.getById(userDto.getUserTypeId()));
    }


    public User fromRegisterDto(RegistrationDto dto, String password) {
        User user = new User();

        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setPassword(password);
        user.setType(userTypeRepository.getById(2));


        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        return userDto;
    }


    public void updateUser(User currentUser, UpdateUserDto updateUserDto) throws NoSuchAlgorithmException, InvalidKeySpecException, NotMatchingEmailException {
        if (updateUserDto.getPhoneNumber() != null && !updateUserDto.getPhoneNumber().isEmpty()) {
            userService.checkForDuplicatesPhoneNumber(updateUserDto.getPhoneNumber());
            currentUser.setPhoneNumber(updateUserDto.getPhoneNumber());
        }

        if (updateUserDto.getNewEmail() != null && !updateUserDto.getNewEmail().isEmpty()) {
            if (!currentUser.getEmail().equals(updateUserDto.getCurrentEmail())) {
                throw new NotMatchingEmailException();
            }
            userService.checkForDuplicatesEmail(updateUserDto.getNewEmail());
            currentUser.setEmail(updateUserDto.getNewEmail());
        }

        if (updateUserDto.getNewPassword() != null && updateUserDto.getNewPassword().isEmpty()) {
            currentUser.setPassword(authenticationHelper.generatePasswordHash(updateUserDto.getNewPassword()));
        }


    }
}
