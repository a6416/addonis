package com.telerikproject.addonis.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDto {

    @NotBlank
    @Size(min = 2, max = 20, message = "Username can't have less than 2 and more than 20 characters")
    private String username;

    @NotBlank
    @Size(min = 8, message = "Password should have at least 8 characters")
    private String password;

    @NotBlank
    private String email;

    @NotBlank
    @Size(min = 10, max = 10, message = "Phone number should have exactly 10 digits")
    private String phoneNumber;

    public UserDto() {
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Positive
    private int userTypeId;

    private boolean blocked;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
