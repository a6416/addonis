package com.telerikproject.addonis.models.dtos;

public class RatingDto {

    int rating;

    public RatingDto() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
