package com.telerikproject.addonis.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AddonDto {

    @NotNull
    @Size(min = 3, max = 30)
    public String name;

    @NotNull
    public String origin;

    @NotNull
    public String description;

    @NotNull
    public int ideId;

    @NotNull
    public String tags;

    public AddonDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdeId() {
        return ideId;
    }

    public void setIdeId(int ideId) {
        this.ideId = ideId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }


}
