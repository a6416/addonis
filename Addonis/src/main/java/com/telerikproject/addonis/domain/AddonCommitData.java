package com.telerikproject.addonis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddonCommitData {

    private AddonCommitter committer;

    private String message;

    public AddonCommitData() {
    }

    public AddonCommitter getCommitter() {
        return committer;
    }

    public void setCommitter(AddonCommitter committer) {
        this.committer = committer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
