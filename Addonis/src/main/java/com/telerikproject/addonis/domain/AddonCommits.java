package com.telerikproject.addonis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddonCommits {

    private AddonCommitData commit;

    public AddonCommits() {
    }

    public AddonCommitData getCommit() {
        return commit;
    }

    public void setCommit(AddonCommitData commit) {
        this.commit = commit;
    }
}
