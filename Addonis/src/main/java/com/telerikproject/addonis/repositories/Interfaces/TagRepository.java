package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.Tag;

import java.util.List;

public interface TagRepository {

    public List<Tag> getAll();

    public Tag getById(int id);

    public Tag getByName(String name);

    public void create(Tag tag);

    public void update(Tag tag);

}
