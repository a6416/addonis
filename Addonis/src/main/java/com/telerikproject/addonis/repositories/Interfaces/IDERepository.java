package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.IDE;

import java.util.List;

public interface IDERepository {

    void create(IDE ide);

    List<IDE> getAll();

    IDE getById(int id);

    IDE getByName(String name);

    void delete(String name);

}
