package com.telerikproject.addonis.repositories;

import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.repositories.interfaces.AddonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class AddonRepositoryImpl implements AddonRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Addon> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon", Addon.class);

            return query.list();
        }
    }

    @Override
    public Addon getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Addon addon = session.get(Addon.class, id);
            if (addon == null) {
                throw new EntityNotFoundException("User", id);
            }
            return addon;
        }
    }

    @Override
    public Addon getByName(String name) {
        try {
            return getAll()
                    .stream()
                    .filter(addon -> addon.getName().replaceAll(" ", "").equals(name))
                    .collect(Collectors.toList())
                    .get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new EntityNotFoundException("Addon", "name", name);
        }
    }

    @Override
    public Addon getByOrigin(String origin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where origin = :origin", Addon.class);
            query.setParameter("origin", origin);

            List<Addon> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Addon", "origin", origin);
            }

            return result.get(0);
        }

    }


    @Override
    public List<Addon> getForUser(String username) {
        return getAll()
                .stream()
                .filter(addon -> addon.getCreatedBy().getUsername().equals(username))
                .collect(Collectors.toList());
    }


    @Override
    public void create(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.save(addon);
        }
    }

    @Override
    public void update(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(addon);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Addon toDel = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(toDel);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Addon> filter(Optional<String> addonName,
                              Optional<Integer> ideId,
                              Optional<String> sortParams) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder stringBuilder = new StringBuilder();
            List<String> filter = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            addonName.ifPresent(value -> {
                if (!value.equals("")) {
                    filter.add(" name like :name ");
                    params.put("name", "%" + value + "%");
                }
            });


            ideId.ifPresent(value -> {
                if (value != -1) {
                    filter.add(" ide_id = :ide");
                    params.put("ide", value);
                }
            });


            if (params.isEmpty()) {
                stringBuilder.append(" from Addon ").append(String.join(" and ", filter));
            } else {
                stringBuilder.append(" from Addon where ").append(String.join(" and ", filter));
            }

            sortParams.ifPresent(value -> {
                if (!value.equals("default")) {
                    String sortParamStr = generateQueryStringFromSortParam(value);
                    stringBuilder.append(sortParamStr);
                }
            });


            System.out.println(stringBuilder);

            Query<Addon> query = session.createQuery(stringBuilder.toString(), Addon.class);
            query.setProperties(params);

            return query.list();

        }

    }

    private String generateQueryStringFromSortParam(String sortParams) {

        switch (sortParams) {
            case "Name, ascending":
                return " order by name asc";
            case "Name, descending":
                return " order by name desc";
            case "Number of downloads, ascending":
                return " order by number_of_downloads asc";
            case "Number of downloads, descending":
                return " order by number_of_downloads desc";
            case "Upload date, ascending":
                return " order by upload_date asc";
            case "Upload date, descending":
                return " order by upload_date desc";
        }
        return "Did not enter switch case.";
    }

}
