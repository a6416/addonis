package com.telerikproject.addonis.repositories;

import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.Rating;
import com.telerikproject.addonis.repositories.interfaces.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
        }
    }

    @Override
    public List<Rating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating", Rating.class);

            return query.list();
        }
    }

    @Override
    public List<Rating> getRatingsForAddon(int id) {
        List<Rating> result = getAll()
                .stream()
                .filter(rt -> rt.getAddon().getId() == id)
                .collect(Collectors.toList());


        if (result.isEmpty()) {
            throw new EntityNotFoundException("Ratings", "addon id", String.valueOf(id));
        }

        return result;
    }

    @Override
    public void update(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int addonId, int userId) {
        List<Rating> rating = getAll()
                .stream()
                .filter(rt -> rt.getAddon().getId() == addonId)
                .filter(rt -> rt.getUser().getId() == userId)
                .collect(Collectors.toList());

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(rating.get(0));
            session.getTransaction().commit();
        }
    }
}
