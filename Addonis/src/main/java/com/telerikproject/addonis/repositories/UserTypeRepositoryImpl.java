package com.telerikproject.addonis.repositories;

import com.telerikproject.addonis.models.UserType;
import com.telerikproject.addonis.repositories.interfaces.UserTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserTypeRepositoryImpl implements UserTypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<UserType> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserType> query = session.createQuery("from UserType", UserType.class);

            return query.list();
        }
    }

    @Override
    public UserType getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserType type = session.get(UserType.class, id);
            if (type == null) {
                throw new com.telerikproject.addonis.exceptions.EntityNotFoundException("UserType", id);
            }
            return type;
        }
    }

}
