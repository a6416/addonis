package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.Addon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddonDownloadRepository extends JpaRepository<Addon,Integer> {
}
