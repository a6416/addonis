package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.UserType;

import java.util.List;

public interface UserTypeRepository {

    public List<UserType> getAll();

    public UserType getById(int id);

}
