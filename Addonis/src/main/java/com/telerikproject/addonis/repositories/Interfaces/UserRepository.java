package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.User;

import java.util.List;


public interface UserRepository {

    public List<User> getAll();

    public User getById(int id);

    public User getByUsername(String username);

    public User getByEmail(String email);

    public User getByPhoneNumber(String phoneNumber);

    public void create(User user);

    public void update(User user);

    public void delete(int id);


}
