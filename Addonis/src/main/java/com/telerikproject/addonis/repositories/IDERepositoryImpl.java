package com.telerikproject.addonis.repositories;

import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.repositories.interfaces.IDERepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class IDERepositoryImpl implements IDERepository {

    private final SessionFactory sessionFactory;

    public IDERepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(IDE ide) {
        try (Session session = sessionFactory.openSession()) {
            session.save(ide);
        }
    }

    @Override
    public List<IDE> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<IDE> query = session.createQuery("from IDE", IDE.class);

            return query.list();
        }
    }

    @Override
    public IDE getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            IDE ide = session.get(IDE.class, id);
            if (ide == null) {
                throw new EntityNotFoundException("IDE", id);
            }
            return ide;
        }
    }

    @Override
    public IDE getByName(String name) {
        try {
            return getAll()
                    .stream()
                    .filter(ide -> ide.getName().replaceAll(" ", "").equals(name))
                    .collect(Collectors.toList())
                    .get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new EntityNotFoundException("Ide", "name", name);
        }
    }

    @Override
    public void delete(String name) {
        List<IDE> ideFiltered = getAll()
                .stream()
                .filter(ide -> ide.getName().equals(name))
                .collect(Collectors.toList());

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(ideFiltered.get(0));
            session.getTransaction().commit();
        }
    }
}
