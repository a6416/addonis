package com.telerikproject.addonis.repositories.interfaces;

import com.telerikproject.addonis.models.Addon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AddonRepository   {

    List<Addon> getAll();

    Addon getById(int id);

    Addon getByName(String name);

    Addon getByOrigin(String origin);

    List<Addon> getForUser(String username);

    void create(Addon addon);

    void update(Addon addon);

    void delete(int id);

    List<Addon> filter(Optional<String> addonName,
                       Optional<Integer> ideId,
                       Optional<String> sortParams);

}
