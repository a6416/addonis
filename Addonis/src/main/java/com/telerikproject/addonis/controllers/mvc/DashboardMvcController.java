package com.telerikproject.addonis.controllers.mvc;

import com.telerikproject.addonis.controllers.AuthenticationHelper;
import com.telerikproject.addonis.exceptions.AuthenticationFailureException;
import com.telerikproject.addonis.exceptions.UnauthorizedOperationException;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.UserType;
import com.telerikproject.addonis.repositories.interfaces.UserTypeRepository;
import com.telerikproject.addonis.services.interfaces.AddonService;
import com.telerikproject.addonis.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class DashboardMvcController {

    private final AddonService addonService;
    private final UserService userService;
    private final UserTypeRepository userTypeRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public DashboardMvcController(AddonService addonService, UserService userService, UserTypeRepository userTypeRepository, AuthenticationHelper authenticationHelper) {
        this.addonService = addonService;
        this.userService = userService;
        this.userTypeRepository = userTypeRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("userTypes")
    public List<UserType> populateUserTypes() {
        return userTypeRepository.getAll();
    }

    @ModelAttribute("isCurrentUserAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getType().getId() == 1;
        }
        return false;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("numberOfPendingForUser")
    public int numberOfPending(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        List<Addon> addonsForUser = addonService.getForUser(user.getUsername());
        return (int) addonsForUser
                .stream()
                .filter(addon -> !addon.isApproved())
                .count();
    }

    @ModelAttribute("numberOfAddonsForUser")
    public int numberOfAddonsForUser(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return addonService.getForUser(user.getUsername()).size();
    }

    @ModelAttribute("numberOfAllAddons")
    public int numberOfAllAddons(HttpSession session) {
        return addonService.getAll().size();
    }

    @GetMapping("/dashboard")
    public String showDashboard(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            int numberOfAllAddons = addonService.getAll().size();
            List<Addon> addonsForUser = addonService.getForUser(user.getUsername());
            model.addAttribute("addons", addonsForUser);
            model.addAttribute("numberOfAllAddons", numberOfAllAddons);
            return "dashboard";

        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }

    }

    @GetMapping("/dashboard/pending")
    public String showPendingAddons(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Addon> addonsForUser = addonService.getPendingAddongsForUser(user.getUsername());
            model.addAttribute("addons", addonsForUser);
            return "dashboard-pending";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/dashboard/users")
    public String showUsersDashboard(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);

            if (user.getType().getId() != 1) {
                throw new UnauthorizedOperationException();
            }

            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            List<Addon> addonsForUser = addonService.getForUser(user.getUsername());

            if (addonsForUser.isEmpty()) {
                model.addAttribute("addonsForCurrentUser", 0);
            } else model.addAttribute("addonsForCurrentUser", addonsForUser.size());

            return "dashboard-users";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/";
        }

    }

    @GetMapping("/dashboard/all-addons")
    public String showAllAddons(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);

            if (user.getType().getId() != 1) {
                throw new UnauthorizedOperationException();
            }

            List<User> users = userService.getAll();
            List<Addon> addonsForUser = addonService.getForUser(user.getUsername());
            List<Addon> addons = addonService.getAll();
            model.addAttribute("users", users);
            model.addAttribute("addonsForCurrentUser", addonsForUser);
            model.addAttribute("addons", addons);
            return "dashboard-all-addons";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/";
        }

    }


}
