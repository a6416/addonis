package com.telerikproject.addonis.controllers.mvc;


import com.telerikproject.addonis.controllers.AuthenticationHelper;
import com.telerikproject.addonis.exceptions.AuthenticationFailureException;
import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.NotMatchingEmailException;
import com.telerikproject.addonis.exceptions.UnauthorizedOperationException;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.UserType;
import com.telerikproject.addonis.models.dtos.UpdateUserDto;
import com.telerikproject.addonis.models.dtos.UserDto;
import com.telerikproject.addonis.models.mappers.UserMapper;
import com.telerikproject.addonis.repositories.interfaces.UserTypeRepository;
import com.telerikproject.addonis.services.interfaces.AddonService;
import com.telerikproject.addonis.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final UserTypeRepository userTypeRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, UserTypeRepository userTypeRepository, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userTypeRepository = userTypeRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("userTypes")
    public List<UserType> populateUserTypes() {
        return userTypeRepository.getAll();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isCurrentUserAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getType().getId() == 1;
        }
        return false;
    }

    @GetMapping("/{username}")
    public String getUser(@PathVariable String username, Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            if (!currentUser.getUsername().equals(username) && currentUser.getType().getId() != 1) {
                throw new AuthenticationFailureException("Access denied.");
            }

            User userToView = userService.getByUsername(username);
            model.addAttribute("isViewedUserAdmin", userToView.getType().getId() == 1);
            model.addAttribute("user", userToView);
            model.addAttribute("updateUser", new UpdateUserDto());
            return "user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/{username}/update")
    public String updateUser(@PathVariable String username,
                             @ModelAttribute UpdateUserDto updateUserDto,
                             BindingResult bindingResult,
                             HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "user";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            if (!currentUser.getUsername().equals(username)) {
                throw new AuthenticationFailureException("Access denied.");
            }

            if (updateUserDto.getCurrentPassword() != null) {
                if (!authenticationHelper.validatePassword(updateUserDto.getCurrentPassword(), currentUser.getPassword())) {
                    throw new AuthenticationFailureException("Incorrect password!");
                }

                if (!updateUserDto.getNewPassword().equals(updateUserDto.getNewPasswordConfirm())) {
                    bindingResult.rejectValue(
                            "newPasswordConfirm",
                            "password_error",
                            "Password confirmation should match password.");
                    return "user";
                }
            }


            User userToUpdate = userService.getByUsername(username);
            userMapper.updateUser(userToUpdate, updateUserDto);
            userService.update(userToUpdate);

            return "redirect:/users/" + username;
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return "redirect:/";
        } catch (DuplicateEntityException | NotMatchingEmailException e) {
            bindingResult.rejectValue("newEmail", "email_error", e.getMessage());
            return "redirect:/users/" + username;
        }
    }

    @PostMapping("/{username}/delete")
    public String deleteUser(@PathVariable String username,
                             @ModelAttribute UpdateUserDto updateUserDto,
                             Model model,
                             HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            if (!currentUser.getUsername().equals(username)) {
                throw new AuthenticationFailureException("Access denied.");
            }

            userService.delete(currentUser);
            return "redirect:/users/" + username;
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/{username}/changeBlockedStatus")
    public String changeBlockedStatus(@PathVariable String username,
                                      HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            if (!currentUser.getUsername().equals(username) && currentUser.getType().getId() != 1) {
                throw new AuthenticationFailureException("Access denied.");
            }

            User userToBlock = userService.getByUsername(username);
            userToBlock.setBlocked(!userToBlock.isBlocked());
            userService.update(userToBlock);

            return "redirect:/dashboard/users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/dashboard/users";
        }
    }

    @GetMapping("/update")
    public String showEditUserPage(@PathVariable String username, Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }

        User user = userService.getByUsername(username);
        UserDto dto = userMapper.toDto(user);

        model.addAttribute("user", dto);
        return "user";
    }

    @GetMapping("/{username}/changeAdminStatus")
    public String changeAdminStatus(@PathVariable String username, HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);

        if (currentUser.getType().getId() != 1) {
            return "404";
        } else {
            User userToUpdate = userService.getByUsername(username);
            userService.changeAdminStatus(userToUpdate);
            return "redirect:/dashboard/users";
        }
    }

}
