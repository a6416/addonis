package com.telerikproject.addonis.controllers.mvc;

import com.google.common.net.HttpHeaders;
import com.telerikproject.addonis.controllers.AuthenticationHelper;
import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.exceptions.NotExpectedFileTypeException;
import com.telerikproject.addonis.exceptions.UnauthorizedOperationException;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.dtos.AddonDto;
import com.telerikproject.addonis.models.dtos.FilterAddonDto;
import com.telerikproject.addonis.models.dtos.RatingDto;
import com.telerikproject.addonis.models.mappers.AddonMapper;
import com.telerikproject.addonis.services.interfaces.AddonService;
import com.telerikproject.addonis.services.interfaces.IDEService;
import com.telerikproject.addonis.services.interfaces.RatingService;
import com.telerikproject.addonis.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/addons")
public class AddonMvcController {

    private final AddonService addonService;
    private final AddonMapper addonMapper;
    private final UserService userService;
    private final RatingService ratingService;
    private final AuthenticationHelper authenticationHelper;
    private final IDEService ideService;


    @Autowired
    public AddonMvcController(AddonService addonService, AddonMapper addonMapper, UserService userService, RatingService ratingService, AuthenticationHelper authenticationHelper, IDEService ideService) {
        this.addonService = addonService;
        this.addonMapper = addonMapper;
        this.userService = userService;
        this.ratingService = ratingService;
        this.authenticationHelper = authenticationHelper;
        this.ideService = ideService;
    }

    @ModelAttribute("ides")
    public List<IDE> populateIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isCurrentUserAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getType().getId() == 1;
        }
        return false;
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of(
                "Name, ascending",
                "Name, descending",
                "Number of downloads, ascending",
                "Number of downloads, descending",
                "Upload date, ascending",
                "Upload date, descending"));
    }

    @GetMapping
    public String get(Model model) {
        List<Addon> addons = addonService.getAll();
        model.addAttribute("addons", addons);
        model.addAttribute("filterAddonDto", new FilterAddonDto());
        return "addons";
    }


    @GetMapping("/{name}")
    public String showOneAddon(@PathVariable String name, Model model, HttpSession session) {

        try {
            Addon addon = addonService.getByName(name);
            model.addAttribute("addon", addon);
            model.addAttribute("ratingDto", new RatingDto());
            model.addAttribute("filterAddonDto", new FilterAddonDto());
            return "addon";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/{name}/delete")
    public String delete(@PathVariable String name, HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        try {
            addonService.delete(name, currentUser);
            return "redirect:/dashboard";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            return "404";
        }
    }

    @GetMapping("/{name}/changeApprovalStatus")
    public String changeApprovalStatus(@PathVariable String name, HttpSession session) {

        User currentUser = authenticationHelper.tryGetUser(session);

        if (currentUser.getType().getId() != 1) {
            return "404";
        } else {
            Addon addonToUpdate = addonService.getByName(name);
            addonService.changeApprovalStatus(addonToUpdate);
            return "redirect:/dashboard/all-addons";
        }
    }

    @GetMapping("/{name}/changeFeaturedStatus")
    public String changeFeaturedStatus(@PathVariable String name, HttpSession session) {

        User currentUser = authenticationHelper.tryGetUser(session);

        if (currentUser.getType().getId() != 1) {
            return "404";
        } else {
            Addon addonToUpdate = addonService.getByName(name);
            addonService.changeFeaturedStatus(addonToUpdate);
            return "redirect:/dashboard/all-addons";
        }
    }

    @GetMapping("/users/{username}")
    public String dashboard(@PathVariable String username, Model model, HttpSession session) {
        List<Addon> addons = addonService.getForUser(username);

        if (addons.isEmpty()) {
            throw new EntityNotFoundException("Addons", "User", username);
        }

        model.addAttribute("addons", addons);

        return "dashboard";
    }

    @GetMapping("/new")
    public String showAddonUploadPage(Model model, HttpSession session) {
        model.addAttribute("addonDto", new AddonDto());
        return "addon-new";
    }

    @PostMapping("/new")
    public String create(@RequestParam("files") MultipartFile file,
                         @Valid @ModelAttribute AddonDto addonDto,
                         BindingResult errors,
                         HttpSession session) {
        if (errors.hasErrors()) {
            return "addon-new";
        }

        try {
            Addon addon = addonMapper.fromDto(file, addonDto, authenticationHelper.tryGetUser(session));
            addonService.create(addon);
            return "redirect:/users/dashboard";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("origin", "addon.exists", e.getMessage());
            return "addon-new";
        } catch (NotExpectedFileTypeException e) {
            errors.rejectValue("tags", "unexpected.file", e.getMessage());
            return "addon-new";
        }
    }


    @GetMapping("/{name}/downloadFile")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String name) {
        Addon addon = addonService.getByName(name);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(addon.getType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\"" + addon.getName() + "\"")
                .body(new ByteArrayResource(addonService.downloadFile(addon)));
    }


    @PostMapping("/filter")
    public String filter(@ModelAttribute FilterAddonDto filterAddonDto, Model model, HttpSession session) {
        List<Addon> addonList = addonService.filter(
                Optional.ofNullable(filterAddonDto.getAddonName()),
                Optional.of(filterAddonDto.getIdeId()),
                Optional.ofNullable(filterAddonDto.getSortParams()));
        if (!filterAddonDto.getAddonName().isBlank()) {
            model.addAttribute("query", ('"' + filterAddonDto.getAddonName() + '"'));
        }
        model.addAttribute("addons", addonList);
        model.addAttribute("numberOfResults", addonList.size());
        return "addons";
    }

    @GetMapping("/showFeatured")
    public String filter(Model model) {
        model.addAttribute("filterAddonDto", new FilterAddonDto());
        model.addAttribute("addons", addonService.showFeatured());
        return "addons";
    }

}
