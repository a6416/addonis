package com.telerikproject.addonis.controllers.rest;

import com.telerikproject.addonis.controllers.AuthenticationHelper;
import com.telerikproject.addonis.exceptions.DuplicateEntityException;
import com.telerikproject.addonis.exceptions.EntityNotFoundException;
import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.User;
import com.telerikproject.addonis.models.dtos.AddonDto;
import com.telerikproject.addonis.models.mappers.AddonMapper;
import com.telerikproject.addonis.services.interfaces.AddonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/addons")
public class AddonController {

    private final AddonService service;
    private final AddonMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AddonController(AddonService service,
                           AddonMapper mapper,
                           AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Addon> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Addon getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Addon create(@RequestParam("files") MultipartFile file,
                        @Valid @RequestBody AddonDto addonDto,
                        HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Addon addon = mapper.fromDto(file, addonDto, user);
            service.create(addon);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/update")
    public String updateAddon(
            @RequestParam("files") MultipartFile file,
            @PathVariable int id,
            @Valid @ModelAttribute("addon") AddonDto addonDto,
            BindingResult errors) {
        if (errors.hasErrors()) {
            return "parcelNew";
        }

        try {
            Addon newAddon = mapper.fromDto(file, addonDto, id);
            service.update(newAddon);
            return "redirect:/addons";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "addon.exists", e.getMessage());
            return "addonNew";
        }
    }


    @GetMapping("/{name}/delete")
    public String deleteAddon(@PathVariable String name, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            service.delete(name, user);
            return "redirect:/addons";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    public List<Addon> showMostPopular() {
        return service.showMostPopular();
    }

    public List<Addon> showFeatured() {
        return service.showFeatured();
    }

    public List<Addon> showNewest() {
        return service.showNewest();
    }

}
