package com.telerikproject.addonis.controllers.mvc;

import com.telerikproject.addonis.models.Addon;
import com.telerikproject.addonis.models.IDE;
import com.telerikproject.addonis.models.dtos.FilterAddonDto;
import com.telerikproject.addonis.services.interfaces.AddonService;
import com.telerikproject.addonis.services.interfaces.IDEService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AddonService service;
    private final IDEService ideService;

    public HomeMvcController(AddonService service, IDEService ideService) {
        this.service = service;
        this.ideService = ideService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("username")
    public String tryGetUsername(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("filterAddonDto")
    public FilterAddonDto generateDto() {
        return new FilterAddonDto();
    }

    @ModelAttribute("ides")
    public List<IDE> populateIDEs() {
        return ideService.getAll();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of(
                "Name, ascending",
                "Name, descending",
                "Number of downloads, ascending",
                "Number of downloads, descending",
                "Upload date, ascending",
                "Upload date, descending"));
    }

    @ModelAttribute("totalDownloadsIntelliJ")
    public int totalDownloadsIntelliJ() {
        List<Addon> downloads = service.filter(Optional.empty(), Optional.of(1), Optional.empty());
        int d = 0;
        for (Addon addon :
                downloads) {
            d += addon.getNumberOfDownloads();
        }
        return d;
    }

    @ModelAttribute("totalDownloadsMVS")
    public int totalDownloadsMVS() {
        List<Addon> downloads = service.filter(Optional.empty(), Optional.of(2), Optional.empty());
        int d = 0;
        for (Addon addon :
                downloads) {
            d += addon.getNumberOfDownloads();
        }
        return d;
    }

    @ModelAttribute("totalDownloadsNetBeans")
    public int totalDownloadsNetBeans() {
        List<Addon> downloads = service.filter(Optional.empty(), Optional.of(3), Optional.empty());
        int d = 0;
        for (Addon addon :
                downloads) {
            d += addon.getNumberOfDownloads();
        }
        return d;
    }

    @ModelAttribute("totalDownloadsEclipse")
    public int totalDownloadsEclipse() {
        List<Addon> downloads = service.filter(Optional.empty(), Optional.of(4), Optional.empty());
        int d = 0;
        for (Addon addon :
                downloads) {
            d += addon.getNumberOfDownloads();
        }
        return d;
    }

    @ModelAttribute("totalDownloadsXCode")
    public int totalDownloadsXCode() {
        List<Addon> downloads = service.filter(Optional.empty(), Optional.of(5), Optional.empty());
        int d = 0;
        for (Addon addon :
                downloads) {
            d += addon.getNumberOfDownloads();
        }
        return d;
    }

    @ModelAttribute("totalNumberIntelliJ")
    public int totalNumberIntelliJ() {
        return service.filter(Optional.empty(), Optional.of(1), Optional.empty()).size();
    }

    @ModelAttribute("totalNumberMVS")
    public int totalNumberMVS() {
        return service.filter(Optional.empty(), Optional.of(2), Optional.empty()).size();
    }

    @ModelAttribute("totalNumberNetBeans")
    public int totalNumberNetBeans() {
        return service.filter(Optional.empty(), Optional.of(3), Optional.empty()).size();
    }

    @ModelAttribute("totalNumberEclipse")
    public int totalNumberEclipse() {
        return service.filter(Optional.empty(), Optional.of(4), Optional.empty()).size();
    }

    @ModelAttribute("totalNumberXCode")
    public int totalNumberXCode() {
        return service.filter(Optional.empty(), Optional.of(5), Optional.empty()).size();
    }


    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("featuredAddons", service.showFeatured());
        return "index";
    }

    @GetMapping("/showPopular")
    public String showFeatured(Model model) {
        model.addAttribute("popularAddons", service.showMostPopular());
        return "index";
    }

    @GetMapping("/showNew")
    public String showNew(Model model) {
        model.addAttribute("newAddons", service.showNewest());
        return "index";
    }

    @GetMapping("/ides/{ideName}")
    public String showAddonsForIde(@PathVariable String ideName, Model model) {
        List<Addon> addons = service.filter(java.util.Optional.empty(),
                Optional.of(ideService.getByName(ideName).getId()),
                java.util.Optional.empty());

        model.addAttribute("addons", addons);
        model.addAttribute("numberOfResults", addons.size());
        return "addons";
    }

}
